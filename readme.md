# UMI Technical Test

## Warming up question
The value printed on the console would be true due to js considering the string as a number ; 567891234 is indeed superior to 7. Moreover, it has nothing to do with the string length, because the string 5678 which is 4 characters long is also superior to 7.

## Functional Requirements

### Characters set available
We should enable them any character as long as it does not require to be encoded and is not a reserved character (doesn't include on its own any meaning ; ex: ? or # have a meaning in uris).
For most countries, this includes : 'A-Z', 'a-z', '0-9', '-', '.', '_', '~'.
It brings up in total 66 characters.
However, given users may want to customize their link, we should maybe enable a more versatile approach and include encoded characters. Nowadays, most browsers enable special characters to be shown inside uris.
While the real uri length is longer do to encoding, users may perceive them on the browser as only one character. Example with Wikipedia :

- ö => %C3%B6 => https://en.wikipedia.org/wiki/M%C3%B6bius_strip => https://en.wikipedia.org/wiki/Möbius_strip
- 关于中文维基百科 => %E5%85%B3%E4%BA%8E%E4%B8%AD%E6%96%87%E7%BB%B4%E5%9F%BA%E7%99%BE%E7%A7%91 => https://zh.wikipedia.org/wiki/Wikipedia:%E5%85%B3%E4%BA%8E%E4%B8%AD%E6%96%87%E7%BB%B4%E5%9F%BA%E7%99%BE%E7%A7%91/en => https://zh.wikipedia.org/wiki/Wikipedia:关于中文维基百科/en

### Uri bounds
- Should bounds be set according to viewed characters or encoded characters ?
- With a minimum consisting of 66 characters, we should use at least 7 characters (66^7 combinations) for generated URIs with the standard set ('A-Z', 'a-z', '0-9', '-', '.', '_', '~'). Less characters implies that uris could be easily spotted. As 66^7 is already a large enough number, maybe more shouldn't be necessary. We also assumes that users uses url shorteners as intended (ie: not using them for an uri already short enough), so we may want to check that the former uri is long enough before generate another one.
- Shorter urls are possible and could be wanted. If a user is willing to pay to customize it's link, he may wants it to be the shortest possible. (ex: umi.us/a).
- When customizing uris, users may want to carry meaning. If so, we should allow an adequate maximum length, maybe 21 characters at most.

### Algorithm for generation
- assert that the former uri is long enough (ignoring 'https://' and '/' as they are mandatory, longer than the domain name length 'umi.us' + 7 characters)
#### If user want to custom its url
- assert that the last part is 7-21 characters long or 1-21 if paid.
- if it's already used, propose alternatives (swapping some characters, find synonyms...)
#### Else (user lets the system generate)
- pick 7 randomly generated characters from the validated set : most of the time, it would be unique.
- check url uniqueness ; if it is already used, retry with swapping one character (randomly or incrementing with ASCII Bounds in mind). It allows us to group uris by closeness/neighbourhood. When the pool gets fewer and fewer uris available via random it would be useful to already forbid the use of some characters at certain places to faster ensure uniqueness, or let the user choose between other propositions.
#### Then
- store the url in db and serve it to the user.

### Expiration
When a link is expired we should still keep its analytics. Should they be showed to users that generated the link or only to the team ? Be aware that if a link has expired it could and hence be used by someone else, we should create separate analytics for the next use.
However a new link to a same former url should always have separated analytics.

### How a request is formed
- https://jamboard.google.com/d/1Uo1dJrVo22SYZaG8om1T9lRk7fDU7Kvyvcniii7Bsb0/viewer?f=0

### What users see
- https://jamboard.google.com/d/1Uo1dJrVo22SYZaG8om1T9lRk7fDU7Kvyvcniii7Bsb0/viewer?f=1