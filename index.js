const LIMIT = 7;

function mistery(testValue, limit = LIMIT) {
    return testValue.toString() > limit;
}

console.log(mistery(567891234));
console.log(mistery(5678));
console.log(mistery(5));